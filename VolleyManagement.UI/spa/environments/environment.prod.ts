export const environment = {
  production: true,
  apiUrl: 'https://volley-mgmt-app.azurewebsites.net/',
  jsonBaseUrl: 'https://volleycontent.blob.core.windows.net/vmscripts/tournamentData/'
};

